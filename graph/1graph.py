import tensorflow as tf

default_graph = tf.get_default_graph()
d_const = tf.constant(15)

second_graph = tf.Graph()
with second_graph.as_default():
          s_const = tf.constant(15)

print( d_const.graph is s_const.graph ) # Даже не смотря на то, что d_const = 15; и s_const = 15, они не равны.
print( d_const.graph is default_graph, s_const.graph is second_graph )