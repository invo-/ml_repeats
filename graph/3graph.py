import tensorflow as tf

graph = tf.Graph()
g_const = tf.constant(0)

default_graph = tf.get_default_graph()
default_const = tf.constant(3)

print( "graph is g_const.graph : ", graph is g_const.graph )
print( "default_graph is g_const,graph : ", default_graph is g_const.graph)
print()
print( "default_graph is default_const.graph : ", default_graph is default_const.graph )
print()

with graph.as_default():
          g_const = tf.constant(5)
          
print ( "graph is g_const.graph : ", graph is g_const.graph )