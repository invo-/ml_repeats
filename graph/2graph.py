import tensorflow as tf

default_graph = tf.get_default_graph()
default_const = tf.constant(666)

second_graph = tf.Graph()
with second_graph.as_default():
          second_const = tf.constant(777)

print( second_graph is second_const.graph, default_const.graph is default_graph )