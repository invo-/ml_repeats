import tensorflow as tf

def_graph = tf.get_default_graph()
def_const = tf.constant(13)

second_graph = tf.Graph()
with second_graph.as_default():
          second_const = tf.constant(14)

# Тут ошибка. Вместо def_const должно быть def_const.graph
print( def_graph is def_const, second_const is second_graph )
print( def_graph is second_const, second_const is def_const )
print( "def_graph is second_graph : ",def_graph is second_graph )
print( "def_const is second_const : ", def_const is second_const )

print( def_graph is def_const.graph, second_const.graph is second_graph ) # True True
print( def_graph is second_const.graph, second_const.graph is def_const ) #  False False
print( "def_graph is second_graph : ", def_graph is second_const.graph ) # False
print( "def_const is second_const : ", def_const.graph is second_const.graph ) # False
