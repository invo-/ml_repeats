import tensorflow as tf

default_graph = tf.get_default_graph()
default_const = tf.constant(1)

s_graph = tf.Graph()
with s_graph.as_default():
          s_const = tf.constant(0)

print( default_graph is default_const.graph, default_graph is s_const.graph )
print( s_graph is s_const.graph, s_const is default_const.graph )