import tensorflow as tf

dflt_graph = tf.get_default_graph()
dflt_const = tf.constant(1)

scnd_graph = tf.Graph()
with scnd_graph.as_default():
          scnd_const = tf.constant(1)

print( scnd_graph is scnd_const.graph, dflt_const.graph is dflt_graph )